# frozen_string_literal: true

# OC Change -- #965 public cloud info page
get '/cloud', to: 'public_cloud#index', as: :public_cloud
