# frozen_string_literal: true

resource :dashboard, controller: 'dashboard', only: [] do
  get :issues, action: :issues_calendar, constraints: lambda { |req| req.format == :ics }
  get :issues
  get :merge_requests
  get :activity

  scope module: :dashboard do
    resources :milestones, only: [:index]
    resources :labels, only: [:index]

    resources :groups, only: [:index]
    resources :snippets, only: [:index]

    resources :todos, only: [:index, :destroy] do
      collection do
        delete :destroy_all
        patch :bulk_restore
      end
      member do
        patch :restore
      end
    end

    resources :projects, only: [:index] do
      collection do
        get :starred

        # OC Change -- add own dashboards and blueprints tabs to /dashboard
        get :dashboards
        get :blueprints
      end
    end
  end

  # OC Change #172, #1064 -- redirect legacy /dashboard/... routes to /user/dashboard
  get 'applications', to: 'dashboard/projects', action: :index
  get 'applications(/*vueroute)', 'dashboard/projects', action: :index
  get 'deployments', to: 'dashboard/projects', action: :index
  get 'deployments(/*vueroute)', 'dashboard/projects', action: :index
  get 'environments', to: 'dashboard/projects', action: :index
  get 'environments/:env', to: 'dashboard/projects', action: :index
  get 'environments(/*vueroute)', 'dashboard/projects', action: :index
  get 'settings', to: 'dashboard/projects', action: :index
  get '*unmatched_route', to: 'dashboard/projects', action: :index

  root to: "dashboard/projects#index"
end
