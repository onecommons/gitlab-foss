# frozen_string_literal: true

# OC Change -- #713 username dasboard routing
resources(:home,
          as: :unfurl_home,
          controller: :unfurl_home,
          param: :username,
          only: [:index, :show]) do

  member do
    scope '-' do
      get :deployments
      get 'deployments(/*vueroute)', action: :deployments
      get :environments
      get 'environments/:env', action: :show_environment
      get 'environments(/*vueroute)', action: :environments
    end
  end
end
