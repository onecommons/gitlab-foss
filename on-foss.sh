set -e
[ -z "$1" ] && { echo 'missing tag argument' ; exit 1; }
newtag=$1
git commit -m"patch from oc"
git tag  -m"patch from oc" $newtag
# note: make sure to follow-tags when pushing: 
git push --follow-tags --no-verify $FOSS_REMOTE
if [ -n "$CNG_TOKEN" ]
then
  curl -X POST -F token=$CNG_TOKEN -F ref=master -F "variables[COMPILE_ASSETS]=true" -F "variables[GITLAB_ASSETS_TAG]=$newtag" -F "variables[GITLAB_REF_SLUG]=$newtag"  -F "variables[GITLAB_VERSION]=$newtag"  ${CNG_SERVER:-https://app.dev.unfurl.cloud}/api/v4/projects/${CNG_PROJECT_ID:-24189702}/trigger/pipeline
fi

# alternative method: deploy using autodeploy tag
# autodeploytag="${newtag:0:5}.$(git show -s --format=%cd+%h --date=format:'%Y%m%d%H%M')"

# to build containers:
# cd CNG 
# git checkout master

# git branch $newtag
# sed ci_files/variables.yml
# git commit -m"$newtag" 
# git push 

# autodeploy tag format: https://gitlab.com/gitlab-org/release/docs/blob/master/general/deploy/auto-deploy.md
# git tag -m'autodeploy' $autodeploytag
# git push --follow-tags
