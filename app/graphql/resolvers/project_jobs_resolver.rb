# frozen_string_literal: true

module Resolvers
  class ProjectJobsResolver < BaseResolver
    include Gitlab::Graphql::Authorize::AuthorizeResource
    include LooksAhead

    type ::Types::Ci::JobType.connection_type, null: true
    authorize :read_build
    authorizes_object!
    extension ::Gitlab::Graphql::Limit::FieldCallCount, limit: 1

    argument :statuses, [::Types::Ci::JobStatusEnum],
              required: false,
              description: 'Filter jobs by status.'

    argument :with_artifacts, ::GraphQL::Types::Boolean,
              required: false,
              description: 'Filter by artifacts presence.'

    # OC Change - allow var_deploy_path to pass through to our finder again
    argument :var_deploy_path, ::GraphQL::Types::String,
              required: false,
              description: 'Filter by deploy path pipeline variable'

    alias_method :project, :object

    # OC Change - not sure why this is necessary
    calls_gitaly!

    # OC Change - pass var_deploy_path
    def resolve_with_lookahead(statuses: nil, with_artifacts: nil, var_deploy_path: nil)
      params = {scope: statuses, with_artifacts: with_artifacts}
      params[:var_deploy_path] = var_deploy_path unless var_deploy_path.nil?
      jobs = ::Ci::JobsFinder.new(
        current_user: current_user, project: project, params: params
      ).execute

      apply_lookahead(jobs)
    end

    private

    def preloads
      {
        previous_stage_jobs_or_needs: [:needs, :pipeline],
        artifacts: [:job_artifacts],
        pipeline: [:user]
      }
    end
  end
end
