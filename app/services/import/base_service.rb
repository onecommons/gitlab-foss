# frozen_string_literal: true

module Import
  class BaseService < ::BaseService
    def initialize(client, user, params)
      @client = client
      @current_user = user
      @params = params

      # OC Change -- mirroring by default
      # TODO: don't enable this by default once we get the settings pane finished
      @params[:enable_mirroring] = true
    end

    def authorized?
      can?(current_user, :import_projects, target_namespace)
    end

    private

    def find_or_create_namespace(namespace, owner)
      namespace = params[:target_namespace].presence || namespace

      return current_user.namespace if namespace == owner

      group = Groups::NestedCreateService.new(current_user, group_path: namespace).execute

      group.errors.any? ? current_user.namespace : group
    rescue StandardError => e
      Gitlab::AppLogger.error(e)

      current_user.namespace
    end

    def project_save_error(project)
      project.errors.full_messages.join(', ')
    end

    def success(project)
      # OC Change
      set_mirror_flag(project) if params[:enable_mirroring]
      explicitly_enable_auto_devops(project)

      super().merge(project: project, status: :success)
    end

    # OC Change -- #863 pull mirroring
    # add custom property to import data json blob
    # to indicate this project is a mirror
    def set_mirror_flag(project)
      project.create_or_update_import_data(data: {
        is_mirror: true, mirror_enabled: true
      })
      project.import_data.save!
    end

    # OC Change #928 -- don't disable auto devops pipeline on intiial failure
    # When ADO is enabled implicitly (i.e. from the global or group setting), a failure in the initial pipeline run will
    # disable ADO. We want to keep it on so that it will still run once the user fixes stuff or adds a manual CI file.
    # If ADO is enabled on the project level, it will stay enabled without us needing to reach into GL internals.
    # Thus, even though we have it enabled globally, also enable it here for imported projects.
    def explicitly_enable_auto_devops(project)
      project.auto_devops_enabled = true
      project.save!
    end

    def track_access_level(import_type)
      Gitlab::Tracking.event(
        self.class.name,
        'create',
        label: 'import_access_level',
        user: current_user,
        extra: { user_role: user_role, import_type: import_type }
      )
    end

    def user_role
      if current_user.id == target_namespace.owner_id
        'Owner'
      else
        access_level = current_user&.group_members&.find_by(source_id: target_namespace.id)&.access_level

        case access_level
        when nil
          'Not a member'
        else
          Gitlab::Access.human_access(access_level)
        end
      end
    end
  end
end
