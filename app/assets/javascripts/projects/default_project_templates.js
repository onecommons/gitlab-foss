import { s__ } from '~/locale';

export default {
  application_blueprint: {
    text: s__('Application Blueprint'),
    icon: '.template-option .icon-application_blueprint',
  },
  terraform_blueprint: {
    text: s__('Terraform Blueprint'),
    icon: '.template-option .icon-terraform_blueprint',
  },
  ansible_blueprint: {
    text: s__('Ansible Blueprint'),
    icon: '.template-option .icon-ansible_blueprint',
  },
  unfurl_testbed: {
    text: s__('Shared Testbed'),
    icon: '.template-option .icon-unfurl_testbed',
  },
  unfurl_dashboard: {
    text: s__('Home Dashboard'),
    icon: '.template-option .icon-unfurl_dashboard',
  }
};
