import { s__ } from '~/locale';

export const GCP_API_ERROR = s__(
  'ClusterIntegration|An error occurred when trying to contact the Google Cloud API. Please try again later.',
);
export const GCP_API_CLOUD_BILLING_ENDPOINT =
  'https://www.googleapis.com/discovery/v1/apis/cloudbilling/v1/rest';
export const GCP_API_CLOUD_RESOURCE_MANAGER_ENDPOINT =
  'https://www.googleapis.com/discovery/v1/apis/cloudresourcemanager/v1/rest';
export const GCP_API_COMPUTE_ENDPOINT =
  'https://www.googleapis.com/discovery/v1/apis/compute/v1/rest';

export const ZONES = [{"name":"us-east1-b"},{"name":"us-east1-c"},{"name":"us-east1-d"},{"name":"us-east4-c"},{"name":"us-east4-b"},{"name":"us-east4-a"},{"name":"us-central1-c"},{"name":"us-central1-a"},{"name":"us-central1-f"},{"name":"us-central1-b"},{"name":"us-west1-b"},{"name":"us-west1-c"},{"name":"us-west1-a"},
  {"name":"us-east5-a"},{"name":"us-east5-b"},{"name":"us-east5-c"},{"name":"us-south1-a"},{"name":"us-south1-b"},{"name":"us-south1-c"},{"name":"us-west2-a"},{"name":"us-west2-b"},{"name":"us-west2-c"},{"name":"us-west3-a"},{"name":"us-west3-b"},{"name":"us-west3-c"},{"name":"us-west4-a"},{"name":"us-west4-b"},{"name":"us-west4-c"},
  {"name":"europe-west4-a"},{"name":"europe-west4-b"},{"name":"europe-west4-c"},{"name":"europe-west1-b"},{"name":"europe-west1-d"},{"name":"europe-west1-c"},{"name":"europe-west3-c"},{"name":"europe-west3-a"},{"name":"europe-west3-b"},{"name":"europe-west2-c"},{"name":"europe-west2-b"},{"name":"europe-west2-a"},
  {"name":"europe-central2-a"},{"name":"europe-central2-b"},{"name":"europe-central2-c"},{"name":"europe-north1-a"},{"name":"europe-north1-b"},{"name":"europe-north1-c"},
  {"name":"europe-southwest1-a"},{"name":"europe-southwest1-b"},{"name":"europe-southwest1-c"},{"name":"europe-west6-a"},{"name":"europe-west6-b"},{"name":"europe-west6-c"},{"name":"europe-west8-a"},{"name":"europe-west8-b"},{"name":"europe-west8-c"},{"name":"europe-west9-a"},{"name":"europe-west9-b"},{"name":"europe-west9-c"},
  {"name":"asia-east1-b"},{"name":"asia-east1-a"},{"name":"asia-east1-c"},{"name":"asia-southeast1-b"},{"name":"asia-southeast1-a"},{"name":"asia-southeast1-c"},{"name":"asia-northeast1-b"},{"name":"asia-northeast1-c"},{"name":"asia-northeast1-a"},{"name":"asia-south1-c"},{"name":"asia-south1-b"},{"name":"asia-south1-a"},
  {"name":"australia-southeast1-b"},{"name":"australia-southeast1-c"},
  {"name":"australia-southeast1-a"},{"name":"southamerica-east1-b"},{"name":"southamerica-east1-c"},
  {"name":"australia-southeast2-a"},{"name":"australia-southeast2-b"},{"name":"australia-southeast2-c"},
  {"name":"southamerica-east1-a"},
  {"name":"asia-east2-a"},{"name":"asia-east2-b"},{"name":"asia-east2-c"},{"name":"asia-northeast2-a"},{"name":"asia-northeast2-b"},{"name":"asia-northeast2-c"},{"name":"asia-northeast3-a"},{"name":"asia-northeast3-b"},{"name":"asia-northeast3-c"},{"name":"asia-south2-a"},{"name":"asia-south2-b"},{"name":"asia-south2-c"},{"name":"asia-southeast2-a"},{"name":"asia-southeast2-b"},{"name":"asia-southeast2-c"},
    { "name": "northamerica-northeast1-a" }, { "name": "northamerica-northeast1-b" }, { "name": "northamerica-northeast1-c" }, { "name": "northamerica-northeast2-a" }, { "name": "northamerica-northeast2-b" }, { "name": "northamerica-northeast2-c" }, { "name": "southamerica-west1-a" }, { "name": "southamerica-west1-b" }, { "name": "southamerica-west1-c" }]

