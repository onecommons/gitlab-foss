import ocGcpAuthentication from 'oc/gcp_authentication' // oc change
import initCreateCluster from '~/create_cluster/init_create_cluster';

// oc change: this is a dirty hack
if(document.querySelector('#js-oc-gcp-authentication')) {
  ocGcpAuthentication(initCreateCluster.bind(null, document, gon))
} else {
  initCreateCluster(document, gon);
}
