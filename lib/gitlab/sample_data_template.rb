# frozen_string_literal: true

module Gitlab
  class SampleDataTemplate < ProjectTemplate
    class << self
      def localized_templates_table
        [
        ].freeze
      end

      def all
        localized_templates_table
      end

      def archive_directory
        Rails.root.join("vendor/sample_data_templates")
      end
    end
  end
end
