# Deploying Unfurl Cloud

We maintain two forks of Gitlab:

* a fork of the main commercial [Gitlab repository](https://gitlab.com/gitlab-org/gitlab) at https://github.com/onecommons/gitlab-oc (this repository).
* a fork of the open source [Gitlab repository](https://gitlab.com/gitlab-org/gitlab-foss) at https://gitlab.com:aszs/gitlab-foss

We do our development in the first one because it has a complete upstream commit history but we can only deploy the second one because we can only deploy the open source bits of Gitlab. 

Therefore we need to sync the repositories when we do a deployment, as follows:

1. From a clean working directory, in the gitlab-oc repo, run `./on-oc.sh [deployment-tag] [path-to-gitlab-foss]`. This will generate a patch of changes from the last tagged commit and apply it to the gitlab-foss repo. A apply might fail or be incomplete in the following cases:
* a patch to a file can't be applied to cleanly
* modified binary files
* new files

In that case you'll need manually update gitlab-foss working directory and then run `git add -v .` 

2. CD to your gitlab-foss working directory and run `./on-foss.sh [deployment-tag]` This will commit, tag and push your repo.

3. Once code is pushed to https://gitlab.com:aszs/gitlab-foss it will kick off a pipeline that starts the deployment process -- see https://github.com/onecommons/production-gitlab/blob/master/README.md for the rest of the steps. 

## Upgrading upstream

To upgrade our repos with the latest upstream code:

1. On gitlab-oc, merge the latest upstream code in the `upstream` branch.
2. Merge `upstream` into `master` (or whichever branch you are working on).
3. Fix any merge conflicts and validate that no major regressions occurred.
4. On the upstream `gitlab-foss`, find the commit that has closest date to the latest upstream `gitlab` commit you merged in the previous step and merge that commit into the `upstream` branch in our `gitlab-foss` repo.
5. On gitlab-oc, on master, create a patch of our changes on top of upstream, excluding any `ee` folders: `git diff --binary upstream -- . :^ee* :^*/ee* > "oc-changes.patch"`.
6. On gitlab-foss, create a new branch off `upstream` and apply that patch: `git apply --index --reject --recount --allow-empty oc-changes.patch`.
7. Commit the changes made by applying the patch and merge the new branch into master using the "ours" merge strategy so that it applies all our changes in the branch:

```
    git merge -s ours master
    git checkout master
    git merge newbranch
```

We use this approach so there is no possibility of merge conflicts on the  `gitlab-foss` repo.
  
## Updating the oc-pages npm package

Most of our front-end code lives in http://github.com:onecommons/unfurl-gui and is included our build via the oc-page npm package. The script `update-oc-pages.sh` updates `yarn.lock` to point to the latest code from that repository by removing and re-adding that package. If you are not working on the main branch and its depends on changes in unfurl-gui, edit that script to point it to the corresponding branch on unfurl-gui.

## Updating project templates

If a project template repository changes run `rake gitlab:update_project_templates` to update the archive that is included in the build. This rake task will download the repo and compress as an archive in `vendor\project_templates`.

The "dashboard" and "testbed" project templates require special care to be updated -- instead of running `rake gitlab:update_project_templates`, see `project-templates/README.md` in http://github.com:onecommons/cloud-dev-ensemble.git for instructions on how to update them.

To add or remove project templates you need to update `gitlab/lib/gitlab/project_template.rb` with a new entry that points to a repository in `https://gitlab.com/onecommons/project-templates/`. You'll also need to edit `app/assets/javascripts/projects/default_project_templates.js`.
