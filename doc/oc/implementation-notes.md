# OneCommons customizations

## Project Environments

We want to associate the cloud provider credentials with an environment. Gitlab acquires and stores those when you create a Kubernetes cluster so we leverage that functionality by creating dummy cluster when adding an environment.

* to support non-Kubernetes "clusters" we add "unfurl" to the `platform_type` enumeration in `app/models/clusters/cluster.rb`
* `oc/views/projects/environments/_form.html.haml` adds an dropdown for choosing a cloud provider 
* `app/controllers/projects/environments_controller.rb` is modified so create() redirects to `-/clusters/new?provider=&env=` if a provider is set.
* `oc/app/views/clusters/clusters/new.html.haml` is added. It only include the gcp form fields if :env is set instead of displaying the new Kubernetes cluster form. In that case it sets platform_type = "unfurl" as a hidden field.
* `app/services/clusters/create_service.rb` execute() is modified to only create the record if platform_type != :kubernetes
* `app/controllers/clusters/clusters_controller.rb` Among other changes, create_gcp() is modified so it redirects to the project overview if the cluster being created is not platform_type == 'kubernetes'.

# The "dashboard" project

New users that register themselves will automatically have a "dashboard" project created, see `app/controllers/registrations_controller.rb` using a project template. 

