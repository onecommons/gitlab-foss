# Development README

For the most part you can rely on https://docs.gitlab.com/ee/development/index.html for documentation on how to develop in this repository.
## Installation

You need to first install the [Gitlab Development Kit](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/README.md) as follows:


    bundle install
    gem install gdk
    git clone https://github.com/onecommons/gitlab-oc.git --branch handover --depth=1 gitlab
    gdk install

See the [GDK home page](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/README.md) for more information.

When checking out gitlab, if you get error about a bad reference with git-lfs just disable it: `cd gitlab; git config lfs.fetchexclude "*"`

Gitlab should be running at `http://localhost:3000`. The default admin login is `root` / `5iveL!fe`
## Our GDK Docker image

As an alternative to installing the GDK you can run our container image of the GDK:

     git clone https://github.com/onecommons/gitlab-oc.git --branch handover --depth=1
     cd gitlab-oc
     docker run -it -v $PWD:/home/gdk/gitlab-development-kit/gitlab -p 3001:3001 -p 3808:3808  registry.gitlab.com/breidenbach.aj/gitlab-development-kit:latest

Gitlab will be running on http://0.0.0.0:3000/ (Using `0.0.0.0` instead of `localhost` is required for the webpack dev server that runs one 3808).

## Front-end development

To ease front-end development, our vue components are contained in a separate `oc-pages` npm package. This package is located a separate repository, `unfurl-gui`, so that these components can be developed without requiring this gitlab app to configured and running.

To run gitlab with your local copy of `unfurl-gui`, use `yarn link` inside your `gitlab-oc` directory:

    yarn link /path/to/your/unfurl-gui/packages/oc-pages

This will modify `package.json` and `package-lock.json`. Do *NOT* commit those changes.

If you are working on a branch that is depends on changes to code in `unfurl-gui`, make sure that code is also on a branch in that repository and update `package.json` to point to that branch, for example:

    "oc-pages": "https://github.com/onecommons/unfurl-gui#workspace=oc-pages&head=MY-BRANCH"

This change you *DO* want to commit to your branch.

This process is automated with the `update-oc-pages.sh` script.

### Using our GDK Docker image

The yarn link command also works with our GDK Docker image, you just need to mount your `unfurl-gui` repository as a volume. This example assumes your current directory has both `gitlab-oc` and `unfurl-gui` repositories as children:

     docker run -it -v $PWD/gitlab-oc:/home/gdk/gitlab-development-kit/gitlab -v $PWD/unfurl-gui:/home/gdk/gitlab-development-kit/unfurl-gui  -p 3000:3000 -p 3808:3808 registry.gitlab.com/breidenbach.aj/gitlab-development-kit:latest
