# Architecture

* Gitlab is a fairly standard Ruby-on-Rails app using the standard layout for example /config/application.rb as the application entry point and /config/routes.rb for routes. See https://guides.rubyonrails.org/configuring.html

* /app/controllers/root_controller.rb redirects "/" to the "home_page_url" application setting if the user is not logged in or a dashboard page if the user is logged in. "Your projects" by default unless "Homepage Content" user preference is set -- see /app/views/profiles/preferences/show.html.haml.
  
* Most pages render the template /app/view/layouts/application.html.haml,
whose main partial is /app/views/layouts/_page.html.haml 

* Vue components live in /app/assets/javascripts/ and follow these conventions: https://docs.gitlab.com/ee/development/fe_guide/vue.html#components-and-store
They are loaded via the index.js files found in /app/assets/javascripts/pages/* (see
https://docs.gitlab.com/ee/development/fe_guide/performance.html#page-specific-javascript)

# Customization

* We are following the pattern used by "Gitlab::ee" and putting any new files we create in the "oc" directory. See `config/initializers/0_inject_enterprise_edition_module.rb` and `config/application.rb`.

* Whenever you make changes to an existing gitlab file (as opposed to one we add) please add a comment like “# oc change: blah blah” so we can easily search for those changes and recognize them as ones we made.



