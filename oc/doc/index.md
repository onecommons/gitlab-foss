### Introduction

#### What is Unfurl Cloud?

Unfurl Cloud is a platform for collaboratively developing cloud applications -- from code to deployment.

We’ve tightly integrated the best open source development tools into our platform, including GitLab, Terraform, and Ansible as well as our own unique technology.

Unfurl Cloud is our first step toward building a free and open cloud. Join a [Testbed project](/onecommons/testbeds) and let's build an open cloud together!

#### Why Unfurl Cloud?

**Accelerate development and minimize tedious setup**

Quickly setup up development environments with instant access to live services running in our testbed cloud. Jump-start your production environment using our low-code UI to deploy our cloud blueprints.

**Share and collaborate**

Publish your cloud blueprints or deploy into our public testbed cloud to make your creations available to everyone.

**Infrastructure-as-Code without writing code**

Your deployment history and all the configuration changes you make in our UI are stored in Git as re-usable Infrastructure-as-Code.

**Avoid lock-in**

* _From cloud providers_: Our cloud blueprints are cloud provider independent.
* _From SaaS services_: With Unfurl Cloud you can easily migrate between self-hosted instances and commercial SaaS services, for example between a self-hosted Redis and their commercial service.
* _From us!_
  - We store all your configuration and settings in git using open standards where possible.
  - Deploy using your own cloud accounts.
  - Our architecture is loosely coupled and follows our open cloud vision. For example, you can run our command-line tool locally without any dependencies on our service.
  - Our entire stack is [open source](https://github.com/onecommons).
### User Guides

If you want to deploy pre-built application blueprints on to your own cloud accounts these guides are for you.






