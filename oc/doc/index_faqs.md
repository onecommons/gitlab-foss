### FAQs

#### How is Unfurl Cloud different from other solutions?

**Reducing Cloud Lock-In.** 

Unfurl Cloud makes it easy to deploy to different cloud providers and 3rd party services using your own cloud accounts. With Unfurl Cloud you can easily migrate between self-hosted instances and commerical SaaS services (for example you can use Unfurl Cloud to deploy a self-managed MongoDB instance and then migrate to MongoDB's Atlas service). And all your configuration is stored in git so you aren't locked-in to Unfurl Cloud either!<br>

**Deploy Popular Software — Fast.** 

We are partnering with open source projects to offer a robust catalog of ready-to-deploy applications.<br>

**Customizable.** 

Our application blueprints are stand-alone Git projects that you can fork and customize or you can build your own. And you deploy anywhere using our open command line tool.<br><br>

#### What cloud providers do you support?
We currently support Amazon Web Services and Google Cloud Platform with support for DigitalOcean and Kubernetes clusters available for selected application blueprints.<br><br>
#### How much does Unfurl Cloud cost?
It is currently free to all users.<br><br>
