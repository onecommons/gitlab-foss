import Vue from 'vue';
import VueApollo from 'vue-apollo';
import createDefaultClient from '~/lib/graphql';
import { resolvers, link, typeDefs} from "oc_vue_shared/graphql";

Vue.use(VueApollo);

export default new VueApollo({
  /*
   * chose the following settings to optimize mapping dashboards to our graphql providers query:
   *
   * wait for 2ms before sending batch
   * batch up to 20 graphql calls (arbitrary)
   * batchDebounce: restart the timer whenever we get another graphql call
  */

  defaultClient: createDefaultClient(resolvers, {link, typeDefs, batchMax: 20, batchInterval: 2, batchDebounce: true})
});
