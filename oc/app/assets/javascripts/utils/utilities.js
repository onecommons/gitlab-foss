
import { __ } from '~/locale';

export const isArray = (a) => {
    return Array.isArray(a);
};

export const isObject = (o) => {
    return o === Object(o) && !isArray(o) && typeof o !== 'function';
};

export const isIterable = (obj) => {
    return isArray(obj)  || isObject(obj);
};

export const checkPropertyIbObject = (obj, property) => {
    // eslint-disable-next-line no-prototype-builtins
    return obj.hasOwnProperty(property);
};

export const transformLodashToPascalCase = (object) => {
    if (!isObject(object)) return object;
    const nArray = Object.keys(object).map(el => {
        const newName = el.split("_").map((word, idx) => {
            let nWord = '';
            if(idx >0) {
                nWord = word.charAt(0).toUpperCase() + word.slice(1);
            }else {
                nWord = word;
            }
            return nWord
        })
        return newName.join("");
    });

    Object.keys(object).map((el, idx) => {
        let tempValue = object[el];
        delete object[el];
        object[nArray[idx]] = tempValue;
    });
    return object;
}
