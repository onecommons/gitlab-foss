// OC Change:  #1256 unfurl deploy badge button
import $ from 'jquery';

let badgeType, badgeCloud, badgeCount
let badgeImage, badgeLink, badgeEmbed

function unfurlBadgeListener() {
  const badgeUrl = new URL(badgeImage[0].firstChild.src);
  const params = badgeUrl.searchParams;

  params.set('type', badgeType.val());
  badgeCloud.prop('checked') ? params.set('cloud', true) : params.delete('cloud');
  badgeCount.prop('checked') ? params.set('count', true) : params.delete('count');

  badgeLink.val(badgeUrl.toString());
  badgeImage[0].firstChild.src = badgeUrl.toString();
  badgeEmbed.val(
    `<a href="${
      badgeUrl.toString().split('/-/deploybutton.svg')[0] /* parse out project url */
    }"><img alt="Deploy with Unfurl Cloud" src="${badgeUrl.toString()}" /></a>`,
  );
}

export default function initUnfurlBadgeUrlBuilder() {
  badgeType = $('#uf-badge-type');
  badgeCloud = $('#uf-badge-cloud');
  badgeCount = $('#uf-badge-count');

  badgeImage = $('#js-uf-badge-img');
  badgeLink = $('#js-uf-badge-link');
  badgeEmbed = $('#js-uf-badge-embed');

  badgeType.on('change', unfurlBadgeListener);
  badgeCloud.on('change', unfurlBadgeListener);
  badgeCount.on('change', unfurlBadgeListener);
}
