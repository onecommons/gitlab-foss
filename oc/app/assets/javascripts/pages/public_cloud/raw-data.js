const container = document.querySelector('div#chart')

const cloudmapUrl = container?.dataset?.cloudmap || '/services/unfurl-server/types?auth_project=onecommons%2Funfurl-types&cloudmap=onecommons/cloudmap&file=dummy-ensemble.yaml'

export default function getData() {
    if(!getData.promise) {
        getData.promise = fetch(cloudmapUrl)
            .then(res => res.json())
    }

    return getData.promise
}
