import Vue from 'vue';
import __ from '~/locale';
import MainComponent from './components/main.vue'

export default (mountClusterComponents, elemId='js-oc-gcp-authentication') => {
  const element = document.getElementById(elemId);

  Vue.prototype.mountClusterComponents = mountClusterComponents
  const props = {...element.dataset}

  return new Vue({
    el: element,
    render(createElement) {
      return createElement(MainComponent, {props});
    },
  });
};
