# frozen_string_literal: true

# OC Change -- #1341
# Worker for pinging Unfurl Server to invalidate cache on git updates.
class ProjectUnfurlCacheWorker
  include ApplicationWorker

  data_consistency :always

  sidekiq_options retry: 3

  feature_category :source_code_management
  urgency :high

  # unfurl-server delete is idempotent and can safely run multiple times
  idempotent!

  def perform(project_id, ref, commit_id)
    return unless unfurl_server_url.present?

    project = Project.find_by(id: project_id)
    return unless project && project.repository.exists?

    commit = project.repository.commit(commit_id)
    return unless commit

    # expire unfurl server cache
    unfurl_file_diffs_for_commit(commit).each do |d|
      removed = d.deleted_file? || !unfurl_file?(d.new_path)

      # by default, GL::HTTP blocks requests to the local network. since this is
      # hitting the unfurl-server container directly, we need to allow local
      # (intra-cluster) requests for this POST.
      Gitlab::HTTP.post(
        "#{unfurl_server_url}/populate_cache",
        query: {
          auth_project: project.path_with_namespace,
          visibility: project.visibility_level_name,
          path: removed ? d.old_path : d.new_path,
          latest_commit: commit.sha,
          branch: ref,
          removed: removed
        },
        allow_local_requests: true
      )
    end
  end

  private

  def unfurl_server_url
    conf = Gitlab.config.extra

    # Config object doesn't have .dig()
    if conf.key? 'unfurl_server_url'
      conf.unfurl_server_url
    # OC Change
    # TODO: patch the chart so we can use our field!
    elsif conf.key? 'one_trust_id'
      conf.one_trust_id
    end
  end

  # we only care about files with significance to Unfurl Server
  def unfurl_file?(filename)
    # see Gitlab::FileDetector
    unfurl_file_types = %i[
      deployment_json
      ensemble_json
      ensemble_template_yaml
      ensemble_yaml
      environments_json
      unfurl_json
      unfurl_types_json
      unfurl_yaml
    ]

    unfurl_file_types.include? Gitlab::FileDetector.type_of(filename)
  end

  def unfurl_file_diffs_for_commit(commit)
    # is or was this file an unfurl_file?
    commit.diffs.diff_files.filter { |d| unfurl_file?(d.old_path) || unfurl_file?(d.new_path) }
  end
end
