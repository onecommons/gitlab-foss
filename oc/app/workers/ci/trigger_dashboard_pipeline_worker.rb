# frozen_string_literal: true

module Ci
  class TriggerDashboardPipelineWorker # rubocop:disable Scalability/IdempotentWorker
    include ApplicationWorker
    include PipelineQueue

    sidekiq_options retry: 3

    queue_namespace :pipeline_creation
    feature_category :pipeline_authoring
    urgency :high
    worker_resource_boundary :cpu

    def perform(pipeline_id, variables)
      Ci::Pipeline.find_by_id(pipeline_id).try do |pipeline|
        Ci::TriggerDashboardPipelineService
          .new(project: pipeline.project, current_user: pipeline.user)
          .execute(pipeline, variables)
      end
    end
  end
end
