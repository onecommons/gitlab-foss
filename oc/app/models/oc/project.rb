# frozen_string_literal: true

module OC
  module Project
    extend ActiveSupport::Concern
    extend ::Gitlab::Utils::Override

    prepended do
      # OC Change #1059 -- use project subscriptions to record needed dashboard triggers
      has_many :upstream_project_subscriptions, class_name: 'OC::Subscriptions::Project', foreign_key: :downstream_project_id, inverse_of: :downstream_project
      has_many :upstream_projects, class_name: 'Project', through: :upstream_project_subscriptions, source: :upstream_project, disable_joins: true
      has_many :downstream_project_subscriptions, class_name: 'OC::Subscriptions::Project', foreign_key: :upstream_project_id, inverse_of: :upstream_project
      has_many :downstream_projects, class_name: 'Project', through: :downstream_project_subscriptions, source: :downstream_project, disable_joins: true
    end

    # recalculate the project type custom attribute
    # rubocop: disable CodeReuse/ActiveRecord
    def refresh_unfurl_type!
      type_attr = custom_attributes.find_or_create_by(key: 'unfurl_type')
      type_attr.value = if repository.ensemble_template_yaml.present?
                          OC::UNFURL_BLUEPRINT
                        elsif repository.unfurl_yaml.present?
                          OC::UNFURL_DASHBOARD
                        else
                          OC::REGULAR_PROJECT
                        end

      type_attr.save!
    end
    # rubocop: enable CodeReuse/ActiveRecord

    def unfurl_blueprint?
      unfurl_type == OC::UNFURL_BLUEPRINT
    end

    def unfurl_dashboard?
      unfurl_type == OC::UNFURL_DASHBOARD
    end

    def regular_project?
      unfurl_type == OC::REGULAR_PROJECT
    end

    # OC change -- pull mirroring
    # don't clear credentials for imported repos after the import is finished,
    # if its a regular project i.e. imported / mirrored repo
    override :remove_import_data
    def remove_import_data
      super unless regular_project?
    end

    # OC Change -- pull mirroring
    # the mirror import sets a flag if mirroring is requested
    def mirror?
      import? && regular_project? && import_data&.data&.dig('is_mirror')
    end

    def mirror_enabled?
      mirror? && import_data&.data&.dig('mirror_enabled')
    end

    private

    # rubocop: disable CodeReuse/ActiveRecord
    def unfurl_type
      type = custom_attributes.find_by(key: 'unfurl_type')

      # force re-cache if attribute is missing
      if type.nil?
        refresh_unfurl_type!
        type = custom_attributes.find_by(key: 'unfurl_type')
      end

      type&.value
    end
    # rubocop: enable CodeReuse/ActiveRecord
  end
end
