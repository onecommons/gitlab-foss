# frozen_string_literal: true

module OC
  module User
    extend ActiveSupport::Concern
    extend ::Gitlab::Utils::Override

    # oc change #604
    prepended do
      after_create :create_dashboard_project
    end

    # OC Change Issue #141 & #537
    # since we'll want to see and do things unavailable to the "advanced" role.
    def advanced
      !admin? && !external?
    end

    def developer?
      !external?
    end

    def dashboards
      ::ProjectsFinder.new(current_user: self, params: { min_access_level: ::Gitlab::Access::MAINTAINER, unfurl_type: OC::UNFURL_DASHBOARD })
        .execute
    end

    # rubocop: disable CodeReuse/ActiveRecord
    def own_dashboard
      # find first dashboard that the user owns
      # sort by time to find first project, since the dashboard is created at user signup
      @own_dashboard ||= dashboards.reorder(:created_at).find { |p| p.owner == self }
    end
    # rubocop: enable CodeReuse/ActiveRecord

    def active_dashboard
      @active_dashboard ||= own_dashboard
    end

    def active_dashboard=(project)
      # TODO: store this in database -- custom attribute? import_data?
      # currently not persistent
      @active_dashboard = project
    end

    private

    # oc change #604
    def create_dashboard_project
      return unless self.human?

      was_external = self.external?

      # external users cannot create projects
      # temporarily set user to not external
      if was_external
        self.external = false
        self.save(validate: false)
      end

      # create dashboard project from template
      ::Projects::CreateService.new(self, OC::DASHBOARD_PROJECT_ON_SIGNUP).execute

      # restore previous external state
      if was_external
        self.external = true
        self.save(validate: false)
      end
    end
  end
end
