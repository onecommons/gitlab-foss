# frozen_string_literal: true

module OC
  # ProjectsFinder
  #
  # Extends ProjectsFinder
  #
  # Added arguments:
  #   params:
  #     namespace: int - id of group to find projects under
  #     unfurl_type: string - filter by unfurl project type (OC::UNFURL_*)
  module ProjectsFinder
    extend ::Gitlab::Utils::Override

    private

    override :filter_projects
    def filter_projects(collection)
      collection = super(collection)
      collection = by_namespace(collection)
      by_unfurl_type(collection)
    end

    # rubocop: disable CodeReuse/ActiveRecord
    def by_namespace(collection)
      if params[:namespace].present?
        collection.where(namespace: params[:namespace].to_i)
      else
        collection
      end
    end
    # rubocop: enable CodeReuse/ActiveRecord

    # rubocop: disable CodeReuse/ActiveRecord
    def by_unfurl_type(collection)
      if params[:unfurl_type].present? && [OC::UNFURL_BLUEPRINT, OC::UNFURL_DASHBOARD].include?(params[:unfurl_type])
        collection.joins(:custom_attributes).where(custom_attributes: { key: 'unfurl_type', value: params[:unfurl_type] })
      else
        collection
      end
    end
    # rubocop: enable CodeReuse/ActiveRecord
  end
end
