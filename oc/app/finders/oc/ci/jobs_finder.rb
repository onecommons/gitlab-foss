# frozen_string_literal: true

# OC change #902 -- filter jobs by ci variables
# query param ?var_foo_path=bar searches for jobs where FOO_PATH variable == 'bar'

module OC
  module Ci
    module JobsFinder
      extend ::Gitlab::Utils::Override

      override :execute
      def execute
        builds = super

        filter_by_variable(builds)
      end

      private

      def filter_by_variable(builds)
        # find all var_* query params
        filter_vars = params.select { |k, v| k.start_with? 'var_' }
          .transform_keys { |k| k.to_s.delete_prefix('var_').upcase }
          # ActionController params wont .to_h unpermitted params by default,
          # but we can only permit if we know what the param will be ahead of
          # time (which we explicitly dont know here). For whatever reason,
          # .to_unsafe_hash doesnt work, but .as_json does what we need
          .as_json

        # search for job ids that have matching variables
        build_ids = builds.select do |b|
          filter_vars.all? { |k, v| b.variables[k]&.value == v }
        end.map(&:id)

        # now fetch AR Collection with the ids
        # we need to do this real hacky with `.where` since the Finder needs to
        # return a AR Collection, and not an array which is what `.select` does.
        # yes, %s is unescaped and unsafe, but we know that the select/map above
        # is only returing ids and this is not handling user input.
        if build_ids.present?
          builds.where('id IN (%s)', build_ids.join(','))
        else
          type.none
        end

        # TODO: replace .select into .where with a single .where with a JOIN
        # running both .select and .where does a double lookup. This is not
        # great and could be faster with a single funky join that does the
        # search on the DB side. However, this is functional.
      end
    end
  end
end

