# frozen_string_literal: true

module OC
  module Git
    module BaseHooksService
      extend ::Gitlab::Utils::Override

      private

      override :enqueue_invalidate_cache
      def enqueue_invalidate_cache
        super

        enqueue_invalidate_unfurl_cache
      end

      def enqueue_invalidate_unfurl_cache
        # trigger Unfurl Server to refresh cache based on latest commit
        ProjectUnfurlCacheWorker.perform_async(project.id, ref, limited_commits.last.id)
      end
    end
  end
end
