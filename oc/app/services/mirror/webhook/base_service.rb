# frozen_string_literal: true

module Mirror
  module Webhook
    class BaseService < ::BaseService
      # OC change -- pull mirroring
      # create a webhook on the upstream Github repository
      # to kick off a fetch whenever upstream is updated
      # webhook POSTs to /projects/:id/mirror/fetch

      attr_reader :project

      def initialize(project, client = nil, params = {})
        @project = project
        @client = client
        @params = params
      end

      private

      def success(hook_data)
        super().merge(hook: hook_data, status: :success)
      end

      def fetch_endpoint_url
        "#{Settings.gitlab.url}/api/v4/projects/#{project.id}/mirror/fetch"
      end

      def generate_validation_token
        token = SecureRandom.hex(20)

        # store the webhook token encrypted with the other import creds
        project.create_or_update_import_data(credentials: {webhook_token: token})
        project.import_data.save!

        token
      end

      def clear_validation_token
        project.create_or_update_import_data(credentials: {webhook_token: nil})
        project.import_data.save!
      end
    end
  end
end
