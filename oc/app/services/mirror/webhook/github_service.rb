# frozen_string_literal: true

module Mirror
  module Webhook
    class GithubService < Mirror::Webhook::BaseService
      # OC change -- pull mirroring
      # create a webhook on the upstream Github repository
      # to kick off a fetch whenever upstream is updated
      # webhook POSTs to /projects/:id/mirror/fetch

      # def initialize(project, client, params)

      attr_accessor :client
      attr_reader :params

      def execute(action)
        case action
        when :create
          new_hook = create()
          success(new_hook)

        when :update
          old_hook = delete()
          new_hook = create()
          success(new_hook)

        when :delete
          old_hook = delete()
          success(old_hook)
        end

      rescue Octokit::Error => e
        log_error(e)
      end

      def repo
        @repo ||= client.repository(params[:repo_name])
      end

      def gh_client
        # legacy client delegates everything, new client needs to call on .octokit
        @gh_client || client.is_a?(Gitlab::LegacyGithubImport::Client) ? client : client.octokit
      end

      def existing_hook
        gh_client.hooks(repo[:id]).find do |hook|
          hook[:config][:url] == fetch_endpoint_url
        end
      end

      def create
        # before doing anything, check if the webhook already exists
        # if so, dont do anything
        return :not_modified if existing_hook

        # recreate webhook with new token
        token = generate_validation_token

        # http://octokit.github.io/octokit.rb/Octokit/Client/Hooks.html#create_hook-instance_method
        gh_client.create_hook(
          repo[:full_name],
          'web',
          {
            url: fetch_endpoint_url,
            content_type: 'json',
            secret: token
          },
          {
            events: ['push'],
            active: true
          }
        )
      end

      def delete
        # before doing anything, check if the webhook already exists
        # if so, dont do anything
        return :not_modified unless existing_hook

        clear_validation_token

        gh_client.remove_hook(repo[:full_name], existing_hook.id)
      end

      def fetch_endpoint_url
        "#{Settings.gitlab.url}/api/v4/projects/#{project.id}/mirror/fetch"
      end

      private

      def log_error(exception)
        error(_('Webhook creation failed due to a GitHub error: %{original}') % { original: exception.response_body }, :unprocessable_entity)
      end
    end
  end
end

Mirror::Webhook::GithubService.prepend_mod_with('Mirror::Webhook::GithubService')
