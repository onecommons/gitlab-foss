# frozen_string_literal: true

module Mirror
  module Webhook
    class ManualService < Mirror::Webhook::BaseService
      # OC change -- pull mirroring
      # create a webhook on the upstream Github repository
      # to kick off a fetch whenever upstream is updated
      # webhook POSTs to /projects/:id/mirror/fetch

      # def initialize(project, user, params)

      def execute(action)
        case action
        when :create, :update
          token = generate_validation_token

          success(hook: {
            url: fetch_endpoint_url,
            secret: validation_token,
            events: ['push']
          })

        when :delete
          clear_validation_token

          success(hook: {
            url: fetch_endpoint_url,
            secret: validation_token,
            events: ['push']
          })
        end
      end
    end
  end
end

Mirror::Webhook::ManualService.prepend_mod_with('Mirror::Webhook::ManualService')
