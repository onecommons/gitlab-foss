# frozen_string_literal: true

module Ci
  class TriggerDashboardPipelineService < BaseProjectService
    include Services::ReturnServiceResponses

    def execute(pipeline, variables = {})
      # project, current_user are class vars

      # trigger all dashboards that are downstream from this
      responses = project.downstream_project_subscriptions.map do |link|
        # add subscription link id to dispatch pipeline variables
        variables[:UPSTREAM_SUBSCRIPTION_ID] = link.id
        downstream = link.downstream_project

        # fetch project token bot user for downstream project
        # bots are always direct members of the project, so just look there
        ds_bot = downstream.members.find { |m| m.user.bot? }&.user || current_user

        # dont use a worker since we need to twiddle the service directly to set the source pipeline
        ::Ci::CreatePipelineService
          .new(downstream, ds_bot, ref: downstream.default_branch,
            variables_attributes: variables.map { |k, v| { key: k.to_s, secret_value: v.to_s } } )
          .execute(:pipeline, ignore_skip_ci: true)
      end

      responses.map! { |r| pipeline_service_response(r.payload) }

      # if any of the pipelines failed to create, set overall error if any fail
      errors = responses.filter { |r| r[:status] == :error }
      if errors.present?
        error('Could not create pipelines', errors.first[:http_status], pipelines: responses)
      else
        success(pipelines: responses)
      end
    end

    private

    # copied from app/services/ci/pipeline_trigger_service.rb
    def pipeline_service_response(pipeline)
      if pipeline.created_successfully?
        success(pipeline: pipeline)
      elsif pipeline.persisted?
        err = pipeline.errors.messages.presence || pipeline.failure_reason.presence || 'Could not create pipeline'
        error(err, :unprocessable_entity)
      else
        error(pipeline.errors.messages, :bad_request)
      end
    end
  end
end
