# frozen_string_literal: true

module Nav
  module TopNavHelper
    PROJECTS_VIEW = :projects
    GROUPS_VIEW = :groups
    NEW_VIEW = :new
    SEARCH_VIEW = :search

    def top_nav_view_model(project:, group:)
      builder = ::Gitlab::Nav::TopNavViewModelBuilder.new

      build_base_view_model(builder: builder, project: project, group: group)

      builder.build
    end

    def top_nav_responsive_view_model(project:, group:)
      builder = ::Gitlab::Nav::TopNavViewModelBuilder.new

      build_base_view_model(builder: builder, project: project, group: group)

      new_view_model = new_dropdown_view_model(project: project, group: group)

      # OC Change
      # if new_view_model && new_view_model.fetch(:menu_sections)&.any?
      #   builder.add_view(NEW_VIEW, new_view_model)
      # end

      # OC Change
      # if top_nav_show_search
      #   builder.add_view(SEARCH_VIEW, ::Gitlab::Nav::TopNavMenuItem.build(**top_nav_search_menu_item_attrs))
      # end

      builder.build
    end

    def build_anonymous_view_model(builder:)
      # OC Change ISSUE-194
      builder.add_primary_menu_item(
        id: 'dashboard',
        title: _('Dashboard'),
        href: new_user_session_path,
        icon: 'home'
      )

      # OC Change ISSUE-194
      builder.add_primary_menu_item(
        id: 'explore',
        title: _('Blueprints'),
        href: explore_blueprints_path,
        icon: 'applications'
      )

      # OC Change ISSUE-194
      builder.add_secondary_menu_item(
        id: 'help',
        title: _('Help'),
        icon: 'question-o',
        href: help_path
      )
    end

    def build_view_model(builder:, project:, group:)
      # These come from `app/views/layouts/nav/_dashboard.html.haml`
      # OC Change ISSUE-194
      builder.add_primary_menu_item(
        id: 'dashboard',
        title: _('Dashboard'),
        href: current_user&.own_dashboard ? project_path(current_user.own_dashboard) : new_project_path,
        icon: 'home',
        css_class: dashboard_section? ? "oc-dropdown-menu-active" : ''
      )

      # OC Change ISSUE-194
      builder.add_primary_menu_item(
        id: 'explore',
        title: _('Blueprints'),
        href: explore_blueprints_path,
        icon: 'applications',
        css_class: explore_section? ? "oc-dropdown-menu-active" : ''
      )
      if dashboard_nav_link?(:projects) && current_user && !current_user.external
        current_item = project ? current_project(project: project) : {}

        builder.add_primary_menu_item_with_shortcut(
          header: top_nav_localized_headers[:switch_to],
          active: nav == 'project' || active_nav_link?(path: %w[root#index projects#trending projects#starred dashboard/projects#index]),
          css_class: 'qa-projects-dropdown',
          data: { track_label: "projects_dropdown", track_action: "click_dropdown" },
          view: PROJECTS_VIEW,
          shortcut_href: dashboard_projects_path,
          **projects_menu_item_attrs
        )
        builder.add_view(PROJECTS_VIEW, container_view_props(namespace: 'projects', current_item: current_item, submenu: projects_submenu))
      end

      if dashboard_nav_link?(:groups) && current_user && !current_user.external
        current_item = group ? current_group(group: group) : {}

        builder.add_primary_menu_item_with_shortcut(
          header: top_nav_localized_headers[:switch_to],
          active: nav == 'group' || active_nav_link?(path: %w[dashboard/groups explore/groups]),
          css_class: 'qa-groups-dropdown',
          data: { track_label: "groups_dropdown", track_action: "click_dropdown" },
          view: GROUPS_VIEW,
          shortcut_href: dashboard_groups_path,
          **groups_menu_item_attrs
        )
        builder.add_view(GROUPS_VIEW, container_view_props(namespace: 'groups', current_item: current_item, submenu: groups_submenu))
      end

      # OC Change ISSUE-194
      builder.add_secondary_menu_item(
        id: 'help',
        title: _('Help'),
        icon: 'question-o',
        href: help_path
      )

      # Using admin? is generally discouraged because it does not check for
      # "admin_mode". In this case we are migrating code and check both, so
      # we should be good.
      # rubocop: disable Cop/UserAdmin
      if current_user&.admin?
        title = _('Admin')

        builder.add_secondary_menu_item(
          id: 'admin',
          title: title,
          active: active_nav_link?(controller: 'admin/dashboard'),
          icon: 'admin',
          href: admin_root_path,
          data: { qa_selector: 'admin_area_link', **menu_data_tracking_attrs(title) }
        )
      end

      if Gitlab::CurrentSettings.admin_mode
        if header_link?(:admin_mode)
          builder.add_secondary_menu_item(
            id: 'leave_admin_mode',
            title: _('Leave Admin Mode'),
            active: active_nav_link?(controller: 'admin/sessions'),
            icon: 'lock-open',
            href: destroy_admin_session_path,
            data: { method: 'post', **menu_data_tracking_attrs('leave_admin_mode') }
          )
        elsif current_user.admin?
          title = _('Enter Admin Mode')

          builder.add_secondary_menu_item(
            id: 'enter_admin_mode',
            title: title,
            active: active_nav_link?(controller: 'admin/sessions'),
            icon: 'lock',
            href: new_admin_session_path,
            data: { qa_selector: 'menu_item_link', qa_title: title, **menu_data_tracking_attrs(title) }
          )
        end
      end
      # rubocop: enable Cop/UserAdmin
    end

    def menu_data_tracking_attrs(label)
      tracking_attrs(
        "menu_#{label.underscore.parameterize(separator: '_')}",
        'click_dropdown',
        'navigation_top'
      )[:data] || {}
    end

Nav::TopNavHelper.prepend_mod
