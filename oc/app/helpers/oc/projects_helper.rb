# frozen_string_literal: true

module OC
  module ProjectsHelper
    extend ::Gitlab::Utils::Override

    # OC Change - #183
    def is_dashboard_project?
      @project&.unfurl_dashboard?
    end
  end
end
