# frozen_string_literal: true

module OC
  module NavHelper
    extend ::Gitlab::Utils::Override

    # OC Change - Issue 151
    def dashboard_section?
      controller.instance_of?(ProjectsController) && @project&.unfurl_dashboard?
    end

    # OC Change - Issue 151
    def explore_section?
      # /explore/blueprints || unfurl blueprint project
      controller.instance_of?(::Explore::BlueprintsController) || @current_file_path == "projects/overview"
    end

    # OC Change - With this controller.class check, the "Help" button will be active when the user is on any help page.
    # Once we decide the exact path for the new user tutorial, we can update this to "current_page?(name_of_path)"
    def help_section?
      controller.instance_of?(HelpController)
    end

    # OC Change - /cloud public cloud page
    def public_cloud_section?
      controller.instance_of?(PublicCloudController)
    end
  end
end
