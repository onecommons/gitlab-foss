# frozen_string_literal: true

module OC
  module UsersHelper
    extend ::Gitlab::Utils::Override

    # OC Change #1000 -- Indicate regular/developer user on profile page
    def user_level_text
      case
      when @user.admin?
        _('Admin')
      when @user.developer?
        _('Developer')
      when @user.external?
        _('Regular')
      else
        _('Unknown')
      end
    end
  end
end
