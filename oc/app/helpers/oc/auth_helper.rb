# frozen_string_literal: true

module OC
  module AuthHelper
    extend ::Gitlab::Utils::Override

    # OC Change -- #985 enable google anylitics for signup
    # the default check in the standard helper checks for Gitlab.com?,
    # and so won't load it for our site. Instead, change to check for oc?.
    override :google_tag_manager_enabled?
    def google_tag_manager_enabled?
      # OC Change #1032 -- hardcode tag manager
      # just enable this unconditionally for now
      ::Gitlab.oc? && ::Gitlab.config.gitlab.host == 'unfurl.cloud'

      # Gitlab.oc? &&
      #   extra_config.has_key?('google_tag_manager_id') &&
      #   extra_config.google_tag_manager_id.present? &&
      #   !current_user
    end
  end
end
