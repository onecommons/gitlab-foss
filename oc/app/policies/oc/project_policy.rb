# frozen_string_literal: true

module OC
  module ProjectPolicy
    extend ActiveSupport::Concern
    extend ::Gitlab::Utils::Override

    prepended do
      desc 'project is an unfurl dashboard'
      condition(:unfurl_dashboard, scope: :subject) { project.unfurl_dashboard? }

      desc 'project is an unfurl blueprint'
      condition(:unfurl_blueprint, scope: :subject) { project.unfurl_blueprint? }

      # OC Change -- #1178
      # expose read_environments for all users who have project access
      rule { can?(:read_project) & unfurl_dashboard }.policy do
        enable :read_environment
        enable :read_deployment
      end
      rule { ~can?(:read_project) & unfurl_dashboard }.policy do
        prevent :read_environment
        prevent :read_deployment
      end

      # OC Change -- #1178
      # use stock prevent conditons read_env/read_deployments for non-board projects
      rule { (builds_disabled & ~unfurl_dashboard) | repository_disabled }.policy do
        prevent :read_environment
        prevent :read_deployment
      end
    end
  end
end
