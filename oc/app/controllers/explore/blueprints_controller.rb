# frozen_string_literal: true

class Explore::BlueprintsController < Explore::ProjectsController
  extend ActiveSupport::Concern
  extend ::Gitlab::Utils::Override
  before_action :welcome_banner

  def index
    response.headers['Link'] = "<#{URI.join(Gitlab.config.gitlab.url, '/explore/blueprints')}>; rel=\"canonical\""
    show_alert_if_search_is_disabled

    # only show "verified" blueprints (official ones from our blueprints group)
    params[:namespace] = ::Group.find_by_full_path(OC::BLUEPRINTS_GROUP)&.id
    @projects = load_projects

    respond_to do |format|
      format.html
      format.json do
        render json: {
          html: view_to_html_string("explore/projects/_projects", projects: @projects)
        }
      end
    end
  end

  def all
    # new route
    @projects = load_projects

    respond_to do |format|
      format.html
      format.json do
        render json: {
          html: view_to_html_string("explore/projects/_projects", projects: @projects)
        }
      end
    end
  end

  private

  # oc change - #87
  def welcome_banner
    @welcome_banner = true
  end

  def load_projects
    # add dashboard filtering to params
    params[:unfurl_type] = OC::UNFURL_BLUEPRINT

    # use existing load method from Explore::ProjectsController
    super
  end
end
