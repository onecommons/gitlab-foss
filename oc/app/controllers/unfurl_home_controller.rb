# frozen_string_literal: true

# OC Change -- #713 username dasboard routing
class UnfurlHomeController < ApplicationController
  # /home should redirect the user to their home dashboard at /:user/dashboard
  # or, redirect them to the login page if not logged in
  def index
    if current_user
      if current_user.own_dashboard
        redirect_to current_user.own_dashboard
      else
        # user does not have dashboard, redirect to create project
        redirect_to new_project_path, alert: _('You do not have a dashboard project! Please create one via the Unfurl Project option.')
      end
    else
      new_user_session_path
    end
  end

  def show
    redirect_to dashboard_project
  end

  def environments
    redirect_to [dashboard_project, :environments]
  end

  def deployments
    redirect_to [dashboard_project, :deployments]
  end

  def show_environment
    redirect_to [dashboard_project, :environments], id: params[:env]
  end

  private

  def dashboard_project
    user = User.find_by(username: params[:username])
    (user || current_user)&.own_dashboard
  end
end
