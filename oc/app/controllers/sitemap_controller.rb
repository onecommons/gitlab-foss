class SitemapController < Explore::BlueprintsController
  skip_before_action :authenticate_user!

  helper_method :loc, :lastmod, :explore_blueprints_loc, :explore_blueprints_lastmod, :cloud_lastmod

  def format_lastmod(date)
    date&.strftime('%Y-%m-%d')
  end

  def loc(project)
    URI.join(Gitlab.config.gitlab.url, project.full_path)
  end

  def lastmod(project)
    format_lastmod(project.last_repository_updated_at)
  end

  def explore_blueprints_loc
    URI.join(Gitlab.config.gitlab.url, explore_blueprints_path)
  end

  def explore_blueprints_lastmod
    last_updated_project = @projects.max_by { |project| project.last_repository_updated_at }
    format_lastmod(last_updated_project&.last_repository_updated_at)
  end

  def cloud_lastmod
    last_updated_project = [@std, @cloudmap].max_by { |project| project&.last_repository_updated_at }
    format_lastmod(last_updated_project&.last_repository_updated_at)
  end

  def show
    # only show "verified" blueprints (official ones from our blueprints group)
    params[:namespace] = ::Group.find_by_full_path(OC::BLUEPRINTS_GROUP)&.id
    @projects = load_projects
    @std = ::Project.find_by_full_path(OC::UNFURL_STD_PROJECT)
    @cloudmap = ::Project::find_by_full_path(OC::UNFURL_CLOUDMAP_PROJECT)

    respond_to do |format|
      format.xml do
        render formats: [:xml], handlers: [:haml]
      end
    end
  end
end
