# frozen_string_literal: true

module OC
  module RootController
    extend ::Gitlab::Utils::Override

    override :redirect_logged_user
    def redirect_logged_user
      # OC Change -- redirect / to user's dashboard or let them create one
      case current_user.dashboard
      when 'projects', 'oc_custom_page'
        if current_user.own_dashboard
          return redirect_to current_user.own_dashboard
        else
          # user does not have dashboard, redirect to create project
          return redirect_to new_project_path, alert: _('You do not have a dashboard project! Please create one via the Unfurl Project option.')
        end
      end

      super
    end
  end
end
