# frozen_string_literal: true

module OC
  class SetupHostingController < ApplicationController
    before_action :authenticate_user!

    def aws
      dashboard = current_user.active_dashboard
      redirect_to("/#{dashboard.full_path}/-/clusters/new?env=#{params[:environment_name]}&provider=aws&arn=arn:aws:iam::#{params[:aws_account]}:role/free-hosting-deploy-role")
    end
  end
end
