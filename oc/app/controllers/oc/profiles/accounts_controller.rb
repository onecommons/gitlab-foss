# frozen_string_literal: true

module OC
  module Profiles::AccountsController
    extend ::Gitlab::Utils::Override

    # OC Change #920, #1301 -- normal/advanced developer selection
    def update
      # TODO: remove if we switch to checkbox instead of role dropdown in form
      params[:user][:external] = params[:user][:role] != 'software_developer'

      result = Users::UpdateService.new(current_user, user_params.merge(user: current_user)).execute

      respond_to do |format|
        if result[:status] == :success
          message = _("User was successfully updated")

          format.html { redirect_back_or_default(default: { action: 'show' }, options: { notice: message }) }
          format.json { render json: { message: message } }
        else
          format.html { redirect_back_or_default(default: { action: 'show' }, options: { alert: result[:message] }) }
          format.json { render json: result }
        end
      end
    end

    private

    def user_params
      params.require(:user).permit(:role, :external)
    end
  end
end
