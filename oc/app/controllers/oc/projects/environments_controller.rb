# frozen_string_literal: true

module OC
  module Projects
    module EnvironmentsController
      extend ActiveSupport::Concern
      extend ::Gitlab::Utils::Override

      # OC Change -- #1064 render unfurl dashboard views over stock project pages

      override :index
      def index
        return super unless @project.unfurl_dashboard? && request.format == :html

        render 'home/index', layout: 'unfurl_home'
      end

      override :show
      def show
        return super unless @project.unfurl_dashboard? && request.format == :html

        render 'home/index', layout: 'unfurl_home'
      end

      private

      override :environment
      def environment
        # allow looking up by environments by name for dashboards
        # other projects and numeric ids handled by stock method
        return super if !@project.unfurl_dashboard? || params[:id] =~ /^\d+$/

        @environment ||= @project.environments.find_by_name(params[:id])
      end
    end
  end
end
