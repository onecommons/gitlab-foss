# frozen_string_literal: true

module OC
  module Projects
    module MirrorsController
      extend ::Gitlab::Utils::Override
      extend ActiveSupport::Concern

      override :update
      def update
        # call stock method if not OC mirror
        return super unless project.mirror?

        project.create_or_update_import_data(data: {
          mirror_enabled: params[:project][:mirror_enabled] == '1'
        })

        if project.save
          flash[:notice] = _('Mirroring settings were successfully updated.')
        else
          flash[:alert] = project.errors.full_messages.join(', ').html_safe
        end

        respond_to do |format|
          format.html { redirect_to_repository_settings(project, anchor: 'js-push-remote-settings') }
          format.json do
            if project.errors.present?
              render json: project.errors, status: :unprocessable_entity
            else
              # render json: ProjectMirrorSerializer.new.represent(project)
              render json: project.import_data.data
            end
          end
        end
      end

      # OC Change #1128 -- manual sync on settings page
      # trigger manual sync from upstream repo from settings page
      override :update_now
      def update_now
        # call stock method if not OC mirror
        return super unless project.mirror?

        project.ensure_repository

        # user's dashboard repo is an import from our dash template,
        # but we dont want to do a fetch from that
        if !project.mirror? || project.sanitized_import_url == "https://gitlab.com/onecommons/project-templates/dashboard.git"
          flash[:error] = 'This project is not a mirrored repo!'
          redirect_to_repository_settings(project, anchor: 'js-push-remote-settings')
          return
        end

        old_head = project.repository.head_commit.id

        # update local project from import source (force pull since we're a mirror)
        project.repository.fetch_as_mirror(project.import_url, forced: true)

        new_head = project.repository.head_commit.id

        # import_url stores credentials so we dont need to build auth header
        # auth_header = upstream_repo_credential_header(project)
        # project.repository.fetch_as_mirror(project.import_url, forced: true, http_authorization_header: auth_header)

        # issue #862
        # after updating repo, kick off ci pipelines if main branch changed
        if new_head != old_head
          CreatePipelineWorker.perform_async(project.id, project.owner.id, new_head, :push)
        end

        flash[:info] = 'Repo updated successfully.'
        redirect_to_repository_settings(project, anchor: 'js-push-remote-settings')
      end
    end
  end
end
