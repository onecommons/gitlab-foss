# frozen_string_literal: true

module OC
  module Projects
    module DeploymentsController
      extend ::Gitlab::Utils::Override
      extend ActiveSupport::Concern

      prepended do
        before_action :authorize_create_pipeline!, only: [:create]
      end

      # OC Change -- #1064 render unfurl dashboard views over stock project pages
      # no override, show does not exist on stock controller
      def show
        return not_found unless project.unfurl_dashboard?

        render 'home/index', layout: 'unfurl_home'
      end

      # OC Change -- #1268 add endpoint for user-initiated / atomic deployments
      def create
        # render not_found doesnt do json, so do it manually
        return render json: { message: "404 project not found" }, status: :not_found unless project.unfurl_dashboard?

        # create and record deploy tokens for upstream projects
        token_results = input_params[:project_dependencies].to_unsafe_h.map do |project_path, var_name|
          setup_deploy_token(project_path, var_name)
        end

        # verify all tokens succeeded
        unless token_results.all? { |t| t[:status] == :success }
          return render json: { message: "failed to create tokens", errors: token_results }, status: :bad_request
        end

        # trigger the pipeline (adapted from projects/pipelines#create)
        service_response = ::Ci::CreatePipelineService
          .new(project, project_bot_user, pipeline_params)
          .execute(:web, ignore_skip_ci: true, save_on_errors: false)

        pipeline = service_response.payload

        if service_response.success?
          render json: PipelineSerializer
                        .new(project: project, current_user: current_user)
                        .represent(pipeline),
                status: :created
        else
          render json: { errors: pipeline.error_messages.map(&:content),
                        warnings: pipeline.warning_messages(limit: ::Gitlab::Ci::Warnings::MAX_LIMIT).map(&:content),
                        total_warnings: pipeline.warning_messages.length },
                status: :bad_request
        end
      end

      private

      def authorize_create_pipeline!
        access_denied! unless can?(current_user, :create_pipeline, project)
      end

      # rubocop: disable CodeReuse/ActiveRecord
      def setup_deploy_token(project_path, var_name)
        # fetch project
        upstream_project = ::Project.find_by_full_path(project_path)
        return { status: :not_found, project: project_path, message: "404 upstream project not found" } unless upstream_project

        # skip if project is public or user/project are internal
        return { status: :success, project: project_path, deploy_token: :public } if upstream_project.public?

        # check permissions (read_repo and read_registry)
        return { status: :error, project: project_path, message: "cannot create deploy token on #{upstream_project.full_path}" } unless
          can?(current_user, :download_code, upstream_project) && can?(current_user, :read_container_image, upstream_project)

        token_params = {
          name: "Unfurl token for #{project.full_path}",
          username: "UNFURL_DEPLOY_TOKEN_#{project.id}",
          expires_at: "",
          read_repository: "1",
          read_registry: "1",
          write_registry: "0",
          read_package_registry: "1",
          write_package_registry: "0"
        }

        # if a token exists and matches what's saved, just return that
        token = upstream_project.deploy_tokens.find_by(username: token_params[:username])&.token
        existing_variable = project.variables.find_by(key: var_name)
        return { status: :success, project: project_path, deploy_token: token } if token && existing_variable&.value == token

        unless token
          # create the token
          result = ::Projects::DeployTokens::CreateService.new(upstream_project, current_user, token_params).execute

          # stop with error if creation did not succeed
          return result.merge(project: project_path) unless result[:status] == :success

          token = result[:deploy_token].token
        end

        var_params = {
          variable_type: :env_var,
          key: var_name,
          secret_value: token,
          masked: true
        }

        # update existing variable, or create new one if doesnt exist
        var = if existing_variable
                ::Ci::ChangeVariableService.new(
                  container: project, current_user: current_user,
                  params: { action: :update, variable: variable, variable_params: var_params }
                ).execute
              else
                ::Ci::ChangeVariableService.new(
                  container: project, current_user: current_user,
                  params: { action: :create, variable_params: var_params }
                ).execute
              end

        if var
          # return deploy token if succeeded
          { status: :success, project: project_path, deploy_token: token }
        else
          # or errors if not
          {
            status: :error, project: project_path,
            message: "cannot save deploy token to variable #{var_name}",
            errors: project.errors&.errors
          }
        end
      end
      # rubocop: enable CodeReuse/ActiveRecord

      def input_params
        params.require(:deployment).permit(:bot_id, :schedule, project_dependencies: {})
      end

      def pipeline_params
        schedule = input_params.dig(:schedule)
        schedule = '0 seconds' if schedule == 'now'

        params.require(:pipeline)[:variables_attributes].push({
          key: "START_IN", variable_type: :env_var, secret_value: schedule
        })

        params.require(:pipeline).permit(:ref, variables_attributes: %i[key variable_type secret_value])
      end

      # rubocop: disable CodeReuse/ActiveRecord
      def project_bot_user
        @project_bot_user ||= ::User.find_by(id: input_params[:bot_id])
      end
      # rubocop: enable CodeReuse/ActiveRecord
    end
  end
end
