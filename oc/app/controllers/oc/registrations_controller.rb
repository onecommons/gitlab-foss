# frozen_string_literal: true

module OC
  module RegistrationsController
    extend ActiveSupport::Concern
    extend ::Gitlab::Utils::Override

    def create
      # OC change -- #672
      # checks that the given invite code is present and valid, otherwise prevent user signup
      # - to update codes, update `UNFURL_VALID_INVITE_CODES` instance CI variable with
      #   a comma, semicolon, or whitespace separated list
      valid_codes_var = ::Ci::InstanceVariable.find_by_key('UNFURL_VALID_INVITE_CODES')&.value
      valid_codes = (valid_codes_var || '').split(/[\s;,]+/)

      code_is_valid = valid_codes.include?(invite_code)

      # only flash error if a bad invite code was present, no code is fine
      if invite_code.present? && !code_is_valid
        flash[:alert] = 'Invite code is invalid.'
        cookies.delete(:invite_code, domain: Settings.gitlab.host) # clear cookie so the error doesnt persist
        render :new
        return
      end

      # use stock method for rest of signup
      super
    rescue ::Gitlab::Access::AccessDeniedError
      redirect_to(new_user_session_path)
    end

    # override :after_request_hook
    # def after_request_hook(user)
    #   super
    # end

    private

    override :set_resource_fields
    def set_resource_fields
      # OC Change: bypass approval for special invite codes
      return if should_approve_without_confirmation?

      super
    end

    override :skip_confirmation?
    def skip_confirmation?
      # OC Change: bypass approval for special invite codes
      super || should_approve_without_confirmation?
    end

    def invite_code
      cookies[:invite_code] || params[:user][:invite_code]
    end

    # OC Change: bypass approval for special invite codes
    # check if invite code matches set string
    def should_approve_without_confirmation?
      # reads regex string from `UNFURL_APPROVE_MATCHING_CODES` instance CI variable
      approved_codes = ::Ci::InstanceVariable.find_by_key('UNFURL_APPROVE_MATCHING_CODES')&.value
      approved_codes.present? && Regexp.new(approved_codes).match?(invite_code)
    end

    override :ensure_first_name_and_last_name_not_empty
    def ensure_first_name_and_last_name_not_empty
      # The key here will be affected by feature flag 'arkose_labs_signup_challenge'
      # When flag is disabled, the key will be 'user' because #check_captcha will remove 'new_' prefix
      # When flag is enabled, #check_captcha will be skipped, so the key will have 'new_' prefix
      first_name = params.dig(resource_name, :first_name) || params.dig("new_#{resource_name}", :first_name)
      last_name = params.dig(resource_name, :last_name) || params.dig("new_#{resource_name}", :last_name)

      # OC Change - optional last name
      return if first_name.present? # && last_name.present?

      resource.errors.add(_('First name'), _("cannot be blank")) if first_name.blank?
      # resource.errors.add(_('Last name'), _("cannot be blank")) if last_name.blank?

      render action: 'new'
    end

  end
end
