# frozen_string_literal: true

module OC
  module Dashboard
    module ProjectsController
      extend ActiveSupport::Concern
      extend ::Gitlab::Utils::Override

      def dashboards
        @projects = load_projects(params.merge(unfurl_type: OC::UNFURL_DASHBOARD))
          .includes(:forked_from_project, :topics)

        respond_to do |format|
          format.html do
            render_projects
          end
          format.atom do
            load_events
            render layout: 'xml'
          end
          format.json do
            render json: {
              html: view_to_html_string("dashboard/projects/_projects", projects: @projects)
            }
          end
        end
      end

      def blueprints
        @projects = load_projects(params.merge(unfurl_type: OC::UNFURL_BLUEPRINT))
          .includes(:forked_from_project, :topics)

        respond_to do |format|
          format.html do
            render_projects
          end
          format.atom do
            load_events
            render layout: 'xml'
          end
          format.json do
            render json: {
              html: view_to_html_string("dashboard/projects/_projects", projects: @projects)
            }
          end
        end
      end

      private

      override :load_projects
      def load_projects(finder_params)
        @all_dashboard_projects = ::ProjectsFinder.new(params: { unfurl_type: OC::UNFURL_DASHBOARD, archived: false, not_aimed_for_deletion: true }, current_user: current_user).execute
        @all_blueprint_projects = ::ProjectsFinder.new(params: { unfurl_type: OC::UNFURL_BLUEPRINT, archived: false, not_aimed_for_deletion: true }, current_user: current_user).execute

        super
      end
    end
  end
end
