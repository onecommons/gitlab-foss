# frozen_string_literal: true

module OC
  module OmniauthCallbacksController
    extend ::Gitlab::Utils::Override

    # OC Change #1302
    # redirect oauth users to welcome page instead of default user page
    def after_sign_in_path_for(resource)
      # set flag for redirect to welcome page for new user
      resource.set_role_required! unless resource.role.present?

      super
    end
  end
end
