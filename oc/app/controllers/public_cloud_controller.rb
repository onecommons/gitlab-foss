# frozen_string_literal: true

# OC Change -- #965 public cloud info page
class PublicCloudController < ApplicationController
  layout 'application'

  # this page should be public
  skip_before_action :authenticate_user!

  def index
    render 'public_cloud/index'
  end
end
