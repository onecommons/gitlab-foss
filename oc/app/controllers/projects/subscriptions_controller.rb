# frozen_string_literal: true

# OC Change #1059 -- use project subscriptions to record needed dashboard triggers
module Projects
  class SubscriptionsController < Projects::ApplicationController
    before_action :ensure_dashboard!
    before_action :authorize_maintainer!

    before_action :upstream_project, only: [:create, :delete]
    before_action :authorize_upstream!, only: [:create, :delete]

    def show
      respond_to do |format|
        format.html
        format.json do
          render json: project.upstream_projects.map { |p| ProjectSerializer.new.represent(p) }
        end
      end
    end

    def create
      # nop if already exists
      if upstream_project_link
        respond_to do |format|
          format.html { redirect_to project, status: :found, notice: _('Upstream project already linked.') }
          format.json { render json: upstream_project_link, status: :ok }
        end
        return
      end

      new_link = project.upstream_project_subscriptions.create(upstream_project: upstream_project)

      respond_to do |format|
        if new_link.persisted?
          format.html { redirect_to project, notice: _('Upstream project linked successfully.') }
          format.json { render json: new_link, status: :created }
        else
          format.html { redirect_to project, notice: _('Failed to link upstream project') }
          format.json { render json: new_link.errors, status: :unprocessable_entity }
        end
      end
    end

    # rubocop: disable CodeReuse/ActiveRecord
    def destroy
      link = project.upstream_project_subscriptions.where(upstream_project: upstream_project).first
      success = link&.destroy

      respond_to do |format|
        if success
          format.html { redirect_to project, status: :found, notice: _('Upstream project removed successfully.') }
          format.json { head :ok }
        else
          format.html { redirect_to project, status: :found, notice: _('Failed to remove upstream project') }
          format.json { head :unprocessable_entity }
        end
      end
    end
    # rubocop: enable CodeReuse/ActiveRecord

    private

    # subscriptions should only be available for dashboard projects
    def ensure_dashboard!
      not_found unless project.unfurl_dashboard?
    end

    def authorize_maintainer!
      not_authorized unless current_user && can?(current_user, :admin_project, project)
    end

    def upstream_project
      @upstream_project ||= find_project(params[:upstream])
    end

    def authorize_upstream!
      not_authorized unless current_user && can?(current_user, :read_container_image, upstream_project)
    end

    def upstream_project_link
      project.upstream_project_subscriptions.where(upstream_project: upstream_project).first
    end

    # from lib/api/helpers.rb
    # :upstream can be project id or path
    # rubocop: disable CodeReuse/ActiveRecord
    def find_project(id)
      projects = Project.without_deleted

      if id.is_a?(Integer) || id =~ /^\d+$/
        projects.find_by(id: id)
      elsif id.include?("/")
        projects.find_by_full_path(id)
      end
    end
    # rubocop: enable CodeReuse/ActiveRecord
  end
end
