# frozen_string_literal: true

module OC
  module InvisibleCaptchaOnSignup
    extend ActiveSupport::Concern

    def on_honeypot_spam_callback
      # OC change -- #672 / noissue
      # invite code registration bypass added to invisible captcha hook
      # this ignores failed captcha for valid invite codes for cypress testing
      valid_codes_var = ::Ci::InstanceVariable.find_by_key('UNFURL_VALID_INVITE_CODES')&.value
      valid_codes = (valid_codes_var || '').split(/[\s;,]+/)

      return if valid_codes.include?(invite_code)

      super
    end

  end
end
