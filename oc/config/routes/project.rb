# frozen_string_literal: true

constraints(::Constraints::ProjectUrlConstrainer.new) do
  scope(path: '*namespace_id',
        as: :namespace,
        namespace_id: ::Gitlab::PathRegex.full_namespace_route_regex) do
    scope(path: ':project_id',
          constraints: { project_id: ::Gitlab::PathRegex.project_route_regex },
          module: :projects,
          as: :project) do
      # Begin of the /-/ scope.
      # Use this scope for all new project routes.
      scope '-' do
        # OC change - add overview route
        get '/overview(/*vueroute)', action: :overview, as: :overview

        # oc change: project home route
        get '/project', action: :home, as: :home

        # OC Change -- #1064 render unfurl dashboard views over stock project pages
        # stock deployments controller is under :project/-/environments/:env/deployments
        # we want it on the project root, so manually add a route for that
        get 'deployments/(/*vueroute)', to: 'deployments#show', as: :deployments
        post 'deployments/new', to: 'deployments#create'

        # OC Change #1059 -- use project subscriptions to record needed dashboard triggers
        resource :subscriptions, only: [:show, :create, :destroy]

        # OC Change #88 #1256 -- deployment count badge
        get 'deploybutton', action: :deploy_status_as_image, as: :deploy_badge

        # oc change: invalidate cache unfurl.json
        get 'invalidate_cache', action: :invalidate_cache
      end

      # OC Change -- #221, et al: blueprint routes
      # TODO: move these under /-/ scope, needs client adjustment
      get 'deployments/(/*vueroute)' => '/projects#overview', as: :blueprint_deployments
      get 'deployment-drafts/(/*vueroute)' => '/projects#overview', as: :blueprint_deployment_drafts
      get 'templates/(/*vueroute)' => '/projects#overview', as: :blueprint_templates
    end
  end
end
