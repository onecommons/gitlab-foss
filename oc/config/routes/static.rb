# OC Change -- #1259 redirect static site urls to static site

%w(
  blog
  about
  developer-guide
  discord
  faq
  guide
  news
  privacypolicy
  terms
).each do |path|
  get "#{path}", to: redirect { |path_params, req| "https://www.unfurl.cloud/#{path}" }, as: :"static_#{path}"
  get "#{path}(/*rest)", to: redirect { |path_params, req| "https://www.unfurl.cloud/#{path}/#{path_params[:rest]}" }
end
