# frozen_string_literal: true

namespace :explore do
  # OC Change #1082 -- separate blueprints from project explore page
  resources :blueprints, only: [:index] do
    collection do
      get :trending
      get :starred
      get :all
    end
  end

  # show blueprints instead of all projects on /explore
  root to: 'blueprints#index'
end
