# frozen_string_literal: true

module Sidebars
  module Projects
    module Menus
      class ApplicationBlueprintMenu < ::Sidebars::Menu

        # override :extra_container_html_options
        # def extra_container_html_options
        #   { class: 'shortcuts-project-information' }
        # end

        override :link
        def link
          project_overview_path(context.project)
        end

        override :extra_nav_link_html_options
        def extra_nav_link_html_options
          { class: 'home' }
        end

        override :title
        def title
          _('Application Blueprint')
        end

        override :active_routes
        def active_routes
          { path: 'projects#overview' }
        end

        override :sprite_icon
        def sprite_icon
          'applications'
        end

        override :render?
        def render?
          true
        end
      end
    end
  end
end