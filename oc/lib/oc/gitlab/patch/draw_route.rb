# frozen_string_literal: true

module OC
  module Gitlab
    module Patch
      module DrawRoute
        extend ::Gitlab::Utils::Override

        override :draw_oc
        def draw_oc(routes_name)
          draw_route(route_path("oc/config/routes/#{routes_name}.rb"))
        end
      end
    end
  end
end
