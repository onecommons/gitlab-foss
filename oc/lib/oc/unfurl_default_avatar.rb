module OC
  class UnfurlDefaultAvatar
    def self.get(project)
      last_digit = project.id.to_s.split("")[-1]
      default_images = {
        "1" => "uf-avatar-placeholder-1.svg",
        "2" => "uf-avatar-placeholder-2.svg",
        "3" => "uf-avatar-placeholder-3.svg",
        "4" => "uf-avatar-placeholder-4.svg",
        "5" => "uf-avatar-placeholder-5.svg",
        "6" => "uf-avatar-placeholder-6.svg",
        "7" => "uf-avatar-placeholder-1.svg",
        "8" => "uf-avatar-placeholder-2.svg",
        "9" => "uf-avatar-placeholder-3.svg",
        "0" => "uf-avatar-placeholder-4.svg",
      }
      return default_images[last_digit]
    end
  end
end