# frozen_string_literal: true

module OC
  module API
    class ProjectMirror < ::API::Base
      extend ActiveSupport::Concern

      feature_category :mirroring

      rescue_from Octokit::Unauthorized, with: :provider_unauthorized

      helpers do
        def project
          # grab project manually by id for webhook, since user_project halper only works with auth token
          @project ||= webhook? ? find_project(params[:id]) : user_project
        end

        def webhook?
          headers['X-Hub-Signature-256'].present?
        end

        def validate_github_webhook(payload)
          # make sure validation token exists
          token = project.import_data&.credentials&.dig(:webhook_token)
          return false unless token.present?

          # perform signature validation (verifies this is the webhook we set up at import)
          signature = 'sha256=' + OpenSSL::HMAC.hexdigest(OpenSSL::Digest.new('sha256'), token, payload)
          Rack::Utils.secure_compare(signature, headers['X-Hub-Signature-256'])
        end

        def gh_client
          @client ||= if Feature.enabled?(:remove_legacy_github_client)
                        ::Gitlab::GithubImport::Client.new(params[:personal_access_token], host: params[:github_hostname])
                      else
                        ::Gitlab::LegacyGithubImport::Client.new(params[:personal_access_token], client_options)
                      end
        end

        def access_params
          { github_access_token: params[:personal_access_token] }
        end

        def client_options
          { host: params[:github_hostname] }
        end

        def provider
          :github
        end

        def provider_unauthorized
          error!("Access denied to your #{::Gitlab::ImportSources.title(provider.to_s)} account.", 401)
        end

        def present_hook(result)
          if result[:status] == :success
            return not_modified! if result[:hook] == :not_modified

            present result[:hook]
          else
            render_api_error!(result[:message], result[:http_status])
          end
        end

        # disable CSRF check for just the fetch endpoint
        # we don't need CSRF for webhooks since we do signature validation instead
        def verified_request?
          route.origin == '/api/:version/projects/:id/mirror/fetch' || ::Gitlab::RequestForgeryProtection.verified?(env)
        end
      end

      params do
        requires :id, type: String, desc: 'The ID of a project'
      end
      resource :projects, requirements: ::API::API::NAMESPACE_OR_PROJECT_REQUIREMENTS do
        desc 'Trigger fetch from configured remote repository'
        post ":id/mirror/fetch" do
          # just ignore payload if mirroring is disabled
          break present(currently_mirroring: false) unless project.mirror_enabled?

          # is it a github webhook?
          if webhook?
            # validate webhook signature
            payload_raw = request.body.read
            forbidden! unless validate_github_webhook(payload_raw)

            payload = ::Gitlab::Json.parse(payload_raw)
            updated_ref = payload['ref'].delete_prefix('refs/head/') if payload['ref']

            # if its a ping webhook, just return and don't actually fetch or trigger ci
            if headers['X-Github-Event'] == 'ping'
              break present ping: 'pong', new_head_commit: project.repository&.head_commit&.id
            end
          else
            # authorize via gitlab token
            authenticate!

            # TODO: allow setting branch name from api param for gl token
          end

          # grab default branch for manual trigger if not set
          updated_ref ||= project.default_branch_or_main

          project.ensure_repository

          # user's dashboard repo is an import from our dash template,
          # but we dont want to do a fetch from that
          if project.sanitized_import_url == "https://gitlab.com/onecommons/project-templates/dashboard.git"
            break render_api_error!('Project is not a remote import', 400)
          end

          # update local project from import source (force pull since we're a mirror)
          begin
            project.repository.fetch_as_mirror(project.import_url, forced: true)
          rescue StandardError => e
            break render_api_error!('Failed to fetch changes', 500)
          end

          # import_url stores credentials so we dont need to build auth header
          # auth_header = upstream_repo_credential_header(project)
          # project.repository.fetch_as_mirror(project.import_url, forced: true, http_authorization_header: auth_header)

          # issue #862
          # after updating repo, kick off ci pipelines
          begin
            CreatePipelineWorker.perform_async(project.id, project.owner.id, updated_ref, :push)
          rescue StandardError
            break render_api_error!('Failed to start pipelines', 500)
          end

          # all good!
          present new_head_commit: project.repository.head_commit.id
        end

        #
        # mirror webbook managment endpoints
        #

        desc 'Manage remote webhooks for pull mirroring'
        params do
          requires :personal_access_token, type: String, desc: 'Github personal access token'
        end
        post ":id/mirror/hook/github" do
          result = Mirror::Webhook::GithubService
            .new(user_project, gh_client, repo_name: user_project.import_source)
            .execute(:create)

          present_hook(result)
        end
        params do
          requires :personal_access_token, type: String, desc: 'Github personal access token'
        end
        delete ":id/mirror/hook/github" do
          result = Mirror::Webhook::GithubService
            .new(user_project, gh_client, repo_name: user_project.import_source)
            .execute(:delete)

          present_hook(result)
        end

        post ":id/mirror/hook/manual" do
          result = Mirror::Webhook::ManualService.new(user_project).execute(:create)

          present_hook(result)
        end
        delete ":id/mirror/hook/manual" do
          result = Mirror::Webhook::ManualService.new(user_project).execute(:delete)

          present_hook(result)
        end
      end
    end
  end
end
