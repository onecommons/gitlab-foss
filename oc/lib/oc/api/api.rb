# frozen_string_literal: true

module OC
  module API
    module API
      extend ActiveSupport::Concern

      prepended do
        mount OC::API::ProjectMirror # OC Change - pull mirroring
      end
    end
  end
end
