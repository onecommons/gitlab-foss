# frozen_string_literal: true

module OC
  module API
    module Projects
      extend ActiveSupport::Concern

      prepended do
        helpers do
          def dashboard_finder_params
            project_finder_params.merge(unfurl_type: OC::UNFURL_DASHBOARD)
          end
        end

        # OC Change #1059 -- add api endpoint to list user's dashboards
        resource :dashboards do
          desc 'Get a list of visible dashboards for authenticated user' do
            success ::API::Entities::BasicProjectDetails
          end
          params do
            use :collection_params
            use :statistics_params

            optional :min_access_level, type: Integer, values: ::Gitlab::Access.all_values, default: ::Gitlab::Access::MAINTAINER,
                                        desc: 'Limit by minimum access level of authenticated user'

            optional :simple, type: ::API::Projects::Boolean, default: false,
                              desc: 'Return only the ID, URL, name, and path of each project'
          end
          get feature_category: :projects do
            present_projects ::ProjectsFinder.new(
              current_user: current_user, params: dashboard_finder_params
            ).execute
          end
        end
      end
    end
  end
end
