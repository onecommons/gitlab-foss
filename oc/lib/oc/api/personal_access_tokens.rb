# frozen_string_literal: true

module OC
  module API
    module PersonalAccessTokens
      extend ActiveSupport::Concern

      prepended do
        # OC Change #1324 -- endpoint to get unfurl user token for git access
        resource :unfurl_access_token do
          desc 'Retrieve an access token for use with Unfurl. Will create one if it does not exist or is expired.' do
            detail 'OC Change'
            success ::API::Entities::PersonalAccessTokenWithToken
          end
          # no params, fetch for current user only
          get do
            return PersonalAccessToken.none unless Ability.allowed?(current_user, :read_user_personal_access_tokens, current_user)

            # lookup existing token from user attributes
            token_attr = current_user.custom_attributes.find_or_create_by(key: 'unfurl_git_token')

            token_id, token_str = token_attr&.value&.split('|')
            token = current_user.personal_access_tokens.find_by(id: token_id.to_i) if token_id.present?

            # create new token if no token or existing is expired
            unless token_id.present? && token&.active?
              token_params = { name: OC::GIT_TOKEN_NAME,
                               scopes: %w(read_repository write_repository),
                               expires_at: 7.days.from_now.to_date.iso8601 }
              response = ::PersonalAccessTokens::CreateService.new(
                current_user: current_user, target_user: current_user, params: token_params
              ).execute

              # error if failed to create token
              return render_api_error!(response.message, response.http_status || :unprocessable_entity) if response.error?

              # save new token on success
              new_token = response.payload[:personal_access_token]

              # OC Change -- #1348 disable notification emails for unfurl git token
              # set these notification flags at create so the notification services
              # will not send out emails (it thinks theyve already been delivered)
              new_token.update(expire_notification_delivered: true, after_expiry_notification_delivered: true)

              token_id, token_str = new_token.id, new_token.token

              # update custom attr with new token details
              token_attr.update!(value: "#{token_id}|#{token_str}")

              # cleanup old token if it was expired (nop if no token)
              token.destroy! if token && !token.active?
            end

            # respond with token and id
            present id: token_id.to_i, token: token_str
          end
        end
      end
    end
  end
end
