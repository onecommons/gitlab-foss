# this script applies any changes made after the last tag to the foss repo
# (excluding ee changes) and then adds a new tag to both repos
set -e
[ -z "$1" ] && { echo "missing argument with the new tag name, last one was $(git describe --first-parent --abbrev=0)" ; exit 1; }
[ -n "$(git status --untracked-files no --porcelain)" ] && { echo "current working dir not clean" ; exit 1; }
newtaglabel=$1
fossdir=${2:-../../gdk-foss/gitlab}
[ -n "$(git -C $fossdir status --porcelain)" ] && { echo "$fossdir working dir not clean" ; exit 1; }

lasttag=$(git describe  --first-parent --abbrev=0)
newtag=$newtaglabel
echo "$lasttag to $newtag"
[ -z "$lasttag" ] && { echo 'missing last tag' ; exit 1; }

git format-patch --binary -o ../patches/$newtag $lasttag -- . :^ee* :^*/ee*
git tag -s -m"patch for foss" $newtag
eedir=$(pwd)

pushd $fossdir
git am $eedir/../patches/$newtag/*.patch
git tag -s -m"patch from oc" $newtag
git push --follow-tags --no-verify $FOSS_REMOTE
popd
